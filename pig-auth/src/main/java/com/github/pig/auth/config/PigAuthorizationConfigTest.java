package com.github.pig.auth.config;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import javax.sql.DataSource;

import static org.mockito.Mockito.*;


/**
 * 功能描述:
 * 
 * @author xiemeng@migu.cn
 * @version [版本号：1.0,日期： 2018/8/14 10:53]
 * @since: 版本号[ ]，更新功能[ ]，更新作者[ ]，更新日期[ ]
 */
public class PigAuthorizationConfigTest
{
    @Mock
    DataSource dataSource;

    @Mock
    AuthenticationManager authenticationManager;

    @Mock
    UserDetailsService userDetailsService;

    @Mock
    RedisConnectionFactory redisConnectionFactory;

    @InjectMocks
    PigAuthorizationConfig pigAuthorizationConfig;

    @Before
    public void setUp()
    {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testConfigure()
        throws Exception
    {

        System.out.println(new BCryptPasswordEncoder().encode("admin"));
        //pigAuthorizationConfig.configure(null);
    }

}